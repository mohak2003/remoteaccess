<!--- Provide a general summary of the issue in the Title above -->

## Description

<!--- Describe your bug in detail -->

#### Expected behavior

#### Actual behavior

#### Steps to reproduce

1.
2.
3.

#### Screenshot / video

<!--Add a screenshot or screencast when applicable-->


## Context

#### Remote access version

<!--You can find it in the About screen-->

#### Port version

<!--Remove the useless ports-->

Android

Windows/Linux

MacOS

iOS
